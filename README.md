# restbed-eventprocessor-amqp

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-eventprocessor-amqp.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-eventprocessor-amqp)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-eventprocessor-amqp/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-eventprocessor-amqp)

This is the AMQP implementation of the EventSender interface for Restbed applications. It utilizes AMQP (RabbitMQ) to send events through a Queue in order to trigger Restbed processors asynchronously.

## Supported tags

* [`1.0.0.M2-73bf5d5`, (*73bf5d5/pom.xml*)](https://bitbucket.org/zuunr/restbed-eventprocessor-amqp/src/73bf5d5/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-eventprocessor-amqp</artifactId>
    <version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for message broker access properties instead of the application.yml file in a non-test environment.

## Integration tests

To run the integration tests, a RabbitMQ runtime must be started. Issue the following docker command to start a RabbitMQ server.

First create the shared docker network:

```shell
docker network create restbed_default
```

Then start the RabbitMQ server:

```shell
docker-compose up
```

After that the RabbitMQEventSenderIT can be executed.
