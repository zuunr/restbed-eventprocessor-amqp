/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.eventprocessor.amqp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.zuunr.restbed.core.processor.Event;
import com.zuunr.restbed.core.processor.EventSender;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@SpringBootApplication(scanBasePackages = {"com.zuunr.restbed.eventprocessor.amqp"})
public class RabbitMQEventSenderIT {
    
    private @Autowired EventSender eventSender;
    private @Autowired MyEventReceiver myEventReceiver;

    @Test
    public void givenEventShouldSendAndReceiveIt() throws InterruptedException, ExecutionException {
        CompletableFuture<Event> futureEvent = new CompletableFuture<>();
        myEventReceiver.setFutureEvent(futureEvent);
        
        Event event = createEvent();
        eventSender.sendPayload(event);
        
        assertEquals(event.asJsonObject(), futureEvent.get().asJsonObject());
    }
    
    private Event createEvent() {
        return Event.builder()
                .href("https://localhost/collection/1")
                .build();
    } 
}
