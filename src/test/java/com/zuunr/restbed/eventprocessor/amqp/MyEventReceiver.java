/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.eventprocessor.amqp;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.processor.Event;
import com.zuunr.restbed.core.processor.EventReceiver;

import reactor.core.publisher.Mono;

@Component
public class MyEventReceiver extends EventReceiver {
    
    private CompletableFuture<Event> futureEvent;
    
    public MyEventReceiver() {
        super(null);
    }
    
    public void setFutureEvent(CompletableFuture<Event> futureEvent) {
        this.futureEvent = futureEvent;
    }
    
    @Override
    public Mono<Void> receive(Mono<Event> event) {
        return event
                .doOnNext(e -> futureEvent.complete(e))
                .then();
    }
}
