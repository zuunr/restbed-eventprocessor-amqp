/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.eventprocessor.amqp;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObjectFactory;
import com.zuunr.restbed.core.processor.DefaultEventSender;
import com.zuunr.restbed.core.processor.Event;
import com.zuunr.restbed.core.processor.EventReceiver;
import com.zuunr.restbed.core.processor.EventSender;

import reactor.core.publisher.Mono;

/**
 * <p>AMQP implementation of {@link EventSender}.</p>
 * 
 * <p>@Primary is added to override the default implementation ({@link DefaultEventSender})
 * in the core module.</p>
 * 
 * @see EventSender
 *
 * @author Mikael Ahlberg
 */
@RabbitListener(queues = "apiQueue")
@Component
@Primary
public class RabbitMQEventSender implements EventSender {
    
    private EventReceiver eventReceiver;
    private RabbitTemplate rabbitTemplate;
    private Queue queue;
    private JsonObjectFactory jsonObjectFactory;
    
    @Autowired
    public RabbitMQEventSender(
            EventReceiver eventReceiver,
            RabbitTemplate rabbitTemplate,
            Queue queue,
            JsonObjectFactory jsonObjectFactory) {
        this.eventReceiver = eventReceiver;
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
        this.jsonObjectFactory = jsonObjectFactory;
    }

    @Override
    public void sendPayload(Event event) {
        rabbitTemplate.convertAndSend(queue.getName(), event.asJsonObject().asJson());
    }
    
    /**
     * <p>Specific receiver method for {@link RabbitHandler}.</p>
     * 
     * <p>Calls the {@link EventReceiver#receive(Mono)} method with the
     * attached json event.</p>
     * 
     * @param eventAsJson is the received event
     */
    @RabbitHandler
    public void receiveRabbit(String eventAsJson) {
        Mono<Event> event = createEvent(eventAsJson);
        
        eventReceiver.receive(event)
            .subscribe();
    }
    
    private Mono<Event> createEvent(String eventAsJsonString) {
        return Mono.just(eventAsJsonString)
                .map(jsonObjectFactory::createJsonObject)
                .map(Event::new);
    } 
}
